/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Fraccion;
import Negocio.MatrizFraccion;
import Util.Impresora;
import com.itextpdf.text.DocumentException;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author estudiante
 */
public class testMatriz {

    public static void main(String[] args) {
        MatrizFraccion m = crearMatriz(3, 3);
        //System.out.println(m);
        Impresora imp = new Impresora(3);
        Impresora imp2 = new Impresora("Todos están felices en Estructuras");
        Impresora imp3 = new Impresora(m);
        Impresora imp4 = new Impresora(new Fraccion(5, 6));

        imp.imprimirConsola();
        imp2.imprimirConsola();
        imp3.imprimirConsola();
        imp4.imprimirConsola();

        try {
            imp.imprimirPDF("Entero", 3 + "", "Esto es un entero");
            imp3.imprimirPDF("Matriz", m, "Esto es una Matriz");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }

    private static MatrizFraccion crearMatriz(int n, int m) {
        MatrizFraccion mm = new MatrizFraccion(n, m);
        for (int i = 0; i < mm.getTamFilas(); i++) {
            for (int j = 0; j < mm.getTamColumnas(); j++) {
                mm.set(i, j, new Random().nextInt(100), new Random().nextInt(200) + 1);

            }
        }
        return mm;
    }
}

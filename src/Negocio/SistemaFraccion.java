/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Fraccion;
import java.util.Arrays;

/**
 *
 * @author docente
 */
public class SistemaFraccion {

    private Fraccion fracciones[];

    public SistemaFraccion() {
    }

    public SistemaFraccion(int n) {

        if (n <= 0) {
            throw new RuntimeException("No se pueden crear Fracciones");
        }

        this.fracciones = new Fraccion[n];
    }
    
    
    public SistemaFraccion(String datos)
    {
    String dd[]=datos.split(",");
    this.fracciones=new Fraccion[dd.length];
        for (int i = 0; i < this.fracciones.length; i++) {
            this.fracciones[i]=new Fraccion(dd[i]);
            
        }
            
    }

    public void set(int i, float num, float den) {
        this.validar(i);
        this.fracciones[i] = new Fraccion(num, den);
    }

    private void validar(int i) {
        if (this.fracciones == null || i < 0 || i >= this.fracciones.length) {
            throw new RuntimeException("Índice fuera de rango");
        }

    }

    public Fraccion get(int i) {
        this.validar(i);
        return this.fracciones[i];
    }

    @Override
    public String toString() {
        String msg = "";
        for (Fraccion f : this.fracciones) {
            msg += f.toString() + "  \t";
        }
        return msg;
    }
    
    /**
     * Ordena usando compareTo
     */
    public void sort()
    {
    
    }
    
    public int length()
    {
        return this.fracciones.length;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Arrays.deepHashCode(this.fracciones);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SistemaFraccion other = (SistemaFraccion) obj;
        
        return true;
    }

    
   
    
}

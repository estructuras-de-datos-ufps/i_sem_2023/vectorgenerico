/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author MADARME
 */
public class Fraccion implements Comparable {

    private float numerador;
    private float denominador;

    public Fraccion() {
    }

    public Fraccion(String datos) {
        String dd[] = datos.split("/");
        this.numerador = Float.parseFloat(dd[0]);
        this.denominador = Float.parseFloat(dd[1]);
    }

    public Fraccion(float numerador, float denominador) {
        this.numerador = numerador;
        if (denominador == 0) {
            throw new RuntimeException("Div 0");
        }
        this.denominador = denominador;
    }

    public float getNumerador() {
        return numerador;
    }

    public void setNumerador(float numerador) {
        this.numerador = numerador;
    }

    public float getDenominador() {
        return denominador;
    }

    public void setDenominador(float denominador) {
        this.denominador = denominador;
    }

    @Override
    public String toString() {
        return numerador + "/" + denominador;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fraccion other = (Fraccion) obj;

        float res = this.getDecimal() - other.getDecimal();
        return res == 0;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Float.floatToIntBits(this.numerador);
        hash = 43 * hash + Float.floatToIntBits(this.denominador);
        return hash;
    }

    @Override
    public int compareTo(Object obj) {
        if (this == obj) {
            return 0;
        }
        if (obj == null) {
            throw new RuntimeException("No se pueden comparar");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("No se pueden comparar");
        }
        final Fraccion other = (Fraccion) obj;

        float res = this.getDecimal() - other.getDecimal();
        if (res == 0) {
            return 0;
        }
        if (res > 0) {
            return 1;
        }
        return -1;

    }

    public float getDecimal() {
        if (this.denominador == 0) {
            throw new RuntimeException("División por ceroF");
        }
        return this.numerador / this.denominador;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 *
 * @author estudiante
 */
public class Impresora<T> {

    private T desconocido;

    public Impresora() {
    }

    public Impresora(T desconocido) {
        this.desconocido = desconocido;
    }

    public void imprimirConsola() {
        System.out.println(desconocido.toString());
    }

    public void imprimirPDF(String nombreArchivo, T informacion, String titulo) throws FileNotFoundException, DocumentException {
        //1.Crear objeto PDF del documento
        Document documento = new Document();
        float id = System.currentTimeMillis();
        nombreArchivo += "-" + id;
        //2. Crear el archivo de almacenamiento--> PDF
        FileOutputStream ficheroPdf = new FileOutputStream("src/pdf/" + nombreArchivo + ".pdf");
        //3. Asignar la estructura del pdf al archivo físico:
        PdfWriter.getInstance(documento, ficheroPdf);
        documento.open();

        //4. Trabajar es crear Párrafos
        Paragraph title = new Paragraph();
        title.add(titulo + "\n");

        Paragraph cuerpo = new Paragraph();
        
        cuerpo.add(informacion.toString());
        
        //Adicionar los párrafos al documento
        documento.add(title);
        documento.add(cuerpo);

        //5. Cerrar
        documento.close();
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Negocio.SistemaFraccion;

/**
 *
 * @author docente
 */
public class TestFraccion {

    public static void main(String[] args) {
        SistemaFraccion s = new SistemaFraccion(3);
        try {
            s.set(0, 1, 7);
            s.set(1, 3, 2);
            s.set(2, 4, 5);
            System.out.println(s);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
